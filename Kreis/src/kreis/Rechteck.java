package kreis;

public class Rechteck {
		//Rechteck Attribute
		private double seiteA, seiteB;
	    
		public Rechteck(double seiteA, double seiteB) {
			this.seiteA = seiteA;
			this.seiteB = seiteB;
		}

		public double getSeiteA() {
			return seiteA;
		}

		public void setSeiteA(double seiteA) {
			this.seiteA = seiteA;
		}

		public double getSeiteB() {
			return seiteB;
		}

		public void setSeiteB(double seiteB) {
			this.seiteB = seiteB;
		}
		
		public double getFl�che() {
			return seiteA*seiteB;
		}
		
		public double getUmfang() {
			return seiteA*2 + 2*seiteB;
		}
		
		
		
}
