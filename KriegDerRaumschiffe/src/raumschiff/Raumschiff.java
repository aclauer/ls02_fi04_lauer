package raumschiff;

import java.util.ArrayList;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**alle Raumschiffe werden mit den gleichen Attributen implemeniert
 * 
 * @author Anna Christina Lauer
 * @version 0.1     
 */

public class Raumschiff {
//	Implementieren aller Attribute für die Klasse Raumschiff
	private static int counter = 1;
	
	private int energieversorgungInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int hülleInProzent;
	private int photonentorpedoAnzahl = 0;
	private int schildeInProzent;
	private int androidenAnzahl = 0;
	private static ArrayList<String> broadcastKommunikator;
	private String schiffsname;
	public ArrayList<Ladung> ladungsverzeichnis;
	
	/**
	 * Erstellt ein neues Raumschiff Objekt ohne Parameter anzunehmen (Konstruktor)
	 */
	public Raumschiff() {
		this.energieversorgungInProzent = 100;
		this.lebenserhaltungssystemeInProzent = 100;
		this.hülleInProzent = 100;
		this.schildeInProzent = 100;
		schiffsname = "RS-Anna-" + counter;
		counter++;
	}
	
/**
 * Konstruktor: nimmt übergebene Parameter an:
 * @param name
 * @param androidenAnzahl
 * @param photonentorpedoAnzahl
 */
	public Raumschiff(String name, int androidenAnzahl, int photonentorpedoAnzahl) {
		super();
		this.energieversorgungInProzent = 100;
		this.lebenserhaltungssystemeInProzent = 100;
		this.hülleInProzent = 100;
		this.schildeInProzent = 100;
		this.schiffsname = name;
		this.androidenAnzahl = androidenAnzahl;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.ladungsverzeichnis = new ArrayList<Ladung>();
		if (this.broadcastKommunikator == null) this.broadcastKommunikator = new ArrayList<String>();
	}	
	
	public static int getCounter() {
		return counter;
	}

	public static void setCounter(int counter) {
		Raumschiff.counter = counter;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getHülleInProzent() {
		return hülleInProzent;
	}

	public void setHülleInProzent(int hülleInProzent) {
		this.hülleInProzent = hülleInProzent;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	/**
	 * In der Konsole werden die Attribute der RS ausgegeben
	 */
	public void zustandAusgeben() {
		System.out.println("Schiffsname: " + "\t\t\t"+schiffsname + "\n"
				+ "Energieversorgung: " + "\t\t"+energieversorgungInProzent + " %\n" 
				+ "Lebenserhaltungssysteme: " + "\t"+lebenserhaltungssystemeInProzent + " %\n"
				+ "Hülle: " + hülleInProzent + " %\n"
				+ "Photonentorpedos: " + "\t\t"+photonentorpedoAnzahl + " Stück\n"
				+ "Schilde: " + "\t\t\t"+schildeInProzent + " %\n"
				+ "Androiden: " + "\t\t\t"+androidenAnzahl + " Stück\n");
	}
	
	/**
	 * 
	 * @return gibt eine ArrayList vom Typ String zurück mit den Logbucheinträgen
	 */
	public ArrayList<String> logbucheintraegeZurueckgeben() {
		return broadcastKommunikator;
	}
	
	/**
	 * senkt Schilde Hülle und Energieversorgung von übergebenem RS
	 * @param raumschiff sagt der Methode welches RS angegriffen wird.
	 */
	public void trefferVermerken(Raumschiff raumschiff) {
		raumschiff.setSchildeInProzent(raumschiff.getSchildeInProzent() - 50);
		if (raumschiff.getSchildeInProzent() < 1) {
			raumschiff.setHülleInProzent(raumschiff.getHülleInProzent() - 50);
			raumschiff.setEnergieversorgungInProzent(raumschiff.getEnergieversorgungInProzent() - 50);
		}
		
		if (raumschiff.getHülleInProzent() < 1) {
			broadcastKommunikator.add(schiffsname + ": Lebenserhaltungssysteme von " + raumschiff.getSchiffsname() + " vernichtet.");
		}
	}
	
	/**
	 * 
	 * @param nachricht übergibt Strings an die ArrayList BroadcastKommunikator
	 */
	public void nachrichtAnAlle(String nachricht) {
		broadcastKommunikator.add(schiffsname + " (" + getTime() + "): " + "\t\t"+nachricht);
	}
	
	/**
	 * gibt das Ladungsverzeichnis auf der Konsole aus 
	 */
	public void ladungsverzeichnisAusgeben() {
		System.out.println("Ladungsverzeichnis: \n");
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			System.out.println(
					ladungsverzeichnis.get(i).getBezeichnung() + "Anzahl: " +ladungsverzeichnis.get(i).getMenge());
		}
	}
	
	/**
	 * die Methode schreibt in den BroadcastKommunikator
	 * die Methode trefferVermerken wird aufgerufen, dieser wird ein RS übergeben.
	 * wenn keine Munition vorhanden, dann Nachricht in BK
	 * @param anzahl - Anzahl an Photonentorpedos die abgeschossen werden.
	 * @param raumschiff - Raumschiff, welches attackiert wird.
	 */
	public void photonentorpedosAbschießen(int anzahl, Raumschiff raumschiff) {
		if (anzahl <= photonentorpedoAnzahl) {
			photonentorpedoAnzahl -= anzahl;
			broadcastKommunikator.add(schiffsname + " (" + getTime() + "): \tPhotonentorpedos abgeschossen");
			trefferVermerken(raumschiff);
		} else {
			broadcastKommunikator.add(schiffsname + " (" + getTime() + "): \t-=*Click*=-");
		}
	}
	
	/**
	 * bei einer Energieversorgung von >=50% wird diese um 50% gesenkt, zudem Nachricht an BK und Aufrufen der Methode trefferVermerken mit Übergabe vom RS
	 * @param raumschiff welches angegriffen werden soll
	 */
	public void phaserkanonenAbschießen(Raumschiff raumschiff) {
		if (energieversorgungInProzent >= 50) {
			energieversorgungInProzent -= 50;
			broadcastKommunikator.add(schiffsname + " (" + getTime() + "): \tPhaserkanone abgeschossen");
			trefferVermerken(raumschiff);
		} else {
			broadcastKommunikator.add(schiffsname + " (" + getTime() + "): \t-=*Click*=-");
		}
	}
	
	/**
	 * @return Gibt die Uhrzeit zurück, hauptsächlich für Broadcast-Log
	 */
	public String getTime() {
		String i = LocalDateTime.now().toString();
		return i.substring(11, 19);
	}
	
	public String getSchiffsname() {
		return schiffsname;
	}
	
	/**
	 * fügt Ladungsobjekte in ArrayList Ladungsverzeichnis hinzu
	 * @param ladung 
	 */
	public void laden(Ladung ladung) {
		this.ladungsverzeichnis.add(ladung);
	}
	
	/**
	 * gibt die String Objekte des BK auf der Konsole aus.
	 */
	public void bcToConsole() {
		System.out.println();
		for(String i : getBroadcastKommunikator()) {
			System.out.println(i);
		}
	}

}
