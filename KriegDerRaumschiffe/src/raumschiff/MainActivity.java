package raumschiff;

public class MainActivity {
	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 2, 1);
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 2, 2);
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 5, 0);

		klingonen.laden(new Ladung("Ferengi Schneckensaft", 200));
		klingonen.laden(new Ladung("Bat'leth Klingonen Schwert", 200));

		romulaner.laden(new Ladung("Borg Schrott", 5));
		romulaner.laden(new Ladung("Rote Materie", 2));
		romulaner.laden(new Ladung("Plasma-Waffe", 50));
		
		vulkanier.laden(new Ladung("Forschungssonde", 35));
		vulkanier.laden(new Ladung("Photonentorpedo", 3));
		
		klingonen.photonentorpedosAbschießen(1, romulaner);
		
		romulaner.phaserkanonenAbschießen(klingonen);
		
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		
		klingonen.zustandAusgeben();
		klingonen.ladungsverzeichnisAusgeben();
		
		klingonen.photonentorpedosAbschießen(2, romulaner);

		romulaner.zustandAusgeben();
		romulaner.ladungsverzeichnisAusgeben();
		
		vulkanier.zustandAusgeben();
		vulkanier.ladungsverzeichnisAusgeben();
		
	
		vulkanier.setEnergieversorgungInProzent(80);
		vulkanier.setSchildeInProzent(80);
		vulkanier.setHülleInProzent(50);
		
	}
}
